import os

CONNECTION_STRING = os.getenv('MONGO_URI')
MONGO_HOST = os.getenv('MONGO_HOST')
MONGO_USER = os.getenv('MONGO_USER')
MONGO_PWD = os.getenv('MONGO_PWD')
MONGO_DB = os.getenv('MONGO_DB')

